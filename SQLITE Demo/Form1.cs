﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLITE_Demo
{
    public partial class Form1 : Form
    {
        private string connectionString = "data source=" + Directory.GetCurrentDirectory() + "\\db\\demo.db;";

        public Form1()
        {
            InitializeComponent();
        }

        private void RefreshRecords()
        {
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(connectionString))
                {
                    connection.Open();

                    // สร้างคำสั่ง SQL สำหรับเลือกข้อมูลจากตาราง
                    string sqlQuery = "SELECT id, title,session FROM tbl_data ORDER BY Updated DESC";

                    // สร้าง DataAdapter เพื่อดึงข้อมูลจาก SQLite ไปยัง DataTable
                    using (SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(sqlQuery, connection))
                    {
                        // สร้าง DataTable เพื่อเก็บข้อมูลที่ได้จาก SQLite
                        DataTable dataTable = new DataTable();
                        dataAdapter.Fill(dataTable);

                        connection.Close();

                        dataGridView1.DataSource = dataTable;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RefreshRecords();
            dataGridView1.ClearSelection();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshRecords();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 frmDialog = new Form2(null);
            frmDialog.ShowDialog();
            if (frmDialog.DialogResult == DialogResult.OK)
            {
                RefreshRecords();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Selected = true;
            string id = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells[0].Value.ToString();
            Form2 frmDialog = new Form2(id);
            frmDialog.ShowDialog();
            if (frmDialog.DialogResult == DialogResult.OK)
            {
                RefreshRecords();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(connectionString))
                {
                    connection.Open();

                    // สร้างคำสั่ง SQL สำหรับเลือกข้อมูลจากตาราง
                    string sqlQuery = "SELECT * FROM tbl_data";

                    // สร้าง DataAdapter เพื่อดึงข้อมูลจาก SQLite ไปยัง DataTable
                    using (SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(sqlQuery, connection))
                    {
                        // สร้าง DataTable เพื่อเก็บข้อมูลที่ได้จาก SQLite
                        connection.Close();
                        stat.Text = "System is Good";
                    }
                }
            }
            catch (Exception ex)
            {
                stat.Text = "System is bad";
            }
        }
    }
}
