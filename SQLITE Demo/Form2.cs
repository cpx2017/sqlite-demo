﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLITE_Demo
{
    public partial class Form2 : Form
    {
        private string gid = null;
        private string connectionString = "data source=" + Directory.GetCurrentDirectory() + "\\db\\demo.db;";

        public Form2(string id)
        {
            InitializeComponent();
            gid = id;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                button1.Text = gid != null ? "Update" : "Save";
                if (gid != null)
                {
                    button2.Visible = true;
                    using (SQLiteConnection connection = new SQLiteConnection(connectionString))
                    {
                        connection.Open();

                        string sqlQuery = "SELECT * FROM tbl_data WHERE id=@id";

                        SQLiteCommand command = new SQLiteCommand(sqlQuery, connection);
                        command.Parameters.AddWithValue("@id", gid);

                        SQLiteDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            textBox1.Text = reader.GetString(reader.GetOrdinal("title"));
                            textBox2.Text = reader.GetString(reader.GetOrdinal("desc"));
                            comboBox1.Text = reader.GetInt32(reader.GetOrdinal("session")).ToString();
                        }
                        reader.Close();
                        connection.Close();
                    }
                }
                else
                {
                    button2.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!comboBox1.Items.Cast<string>().ToArray().Contains(comboBox1.Text))
                {
                    throw new Exception("Session Status is invalid");
                }
                SQLiteConnection connection = new SQLiteConnection(connectionString);
                if (gid != null)
                {
                    connection.Open();

                    string query = "UPDATE tbl_data SET title = @title, desc = @desc, session = @session, Updated = @u WHERE id = @id;";
                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    command.Parameters.AddWithValue("@id", gid);
                    command.Parameters.AddWithValue("@title", textBox1.Text);
                    command.Parameters.AddWithValue("@desc", textBox2.Text);
                    command.Parameters.AddWithValue("@session", Convert.ToInt32(comboBox1.Text));
                    command.Parameters.AddWithValue("@u", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    command.ExecuteNonQuery();

                    connection.Close();
                }
                else
                {
                    connection.Open();

                    string query = "INSERT INTO tbl_data (id, title, desc, session, Updated) VALUES (@id, @title, @desc, @session, @u);";
                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    command.Parameters.AddWithValue("@id", Guid.NewGuid().ToString());
                    command.Parameters.AddWithValue("@title", textBox1.Text);
                    command.Parameters.AddWithValue("@desc", textBox2.Text);
                    command.Parameters.AddWithValue("@session", Convert.ToInt32(comboBox1.Text));
                    command.Parameters.AddWithValue("@u", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    command.ExecuteNonQuery();

                    connection.Close();
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to delete this record", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                try
                {
                    SQLiteConnection connection = new SQLiteConnection(connectionString);

                    connection.Open();

                    string query = "DELETE  FROM tbl_data WHERE id = @id;";
                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    command.Parameters.AddWithValue("@id", gid);

                    command.ExecuteNonQuery();

                    this.DialogResult = DialogResult.OK;

                    connection.Close();

                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
